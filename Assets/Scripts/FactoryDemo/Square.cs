﻿using UnityEngine;

namespace FactoryDemo
{
    public class Square: IShape
    {
        public void Draw()
        {
            Debug.Log($"Inside Square Draw() method");
        }
    }
}