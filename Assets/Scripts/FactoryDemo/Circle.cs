﻿using UnityEngine;

namespace FactoryDemo
{
    public class Circle: IShape
    {
        public void Draw()
        {
            Debug.Log($"Inside Circle Draw() method");
        }
    }
}