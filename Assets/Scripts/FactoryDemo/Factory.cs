﻿using System;
using UnityEngine;

namespace FactoryDemo
{
    public class Factory: MonoBehaviour
    {
        private ShapeFactory m_factory;

        private void Start()
        {
            m_factory = new ShapeFactory();
            var shape1 = ShapeFactory.GETShape("CIRCLE");
            shape1.Draw();
            
            var shape2 = ShapeFactory.GETShape("RECTANGLE");
            shape2.Draw();
            
            var shape3 = ShapeFactory.GETShape("SQUARE");
            shape3.Draw();
            
        }
    }
}