﻿using System;

namespace FactoryDemo
{
    public class ShapeFactory
    {
        public static IShape GETShape(string shapeType)
        {
            return shapeType switch
            {
                null => null,
                "CIRCLE" => new Circle(),
                "RECTANGLE" => new Rectangle(),
                "SQUARE" => new Square(),
                _ => null
            };
        }
    }
}