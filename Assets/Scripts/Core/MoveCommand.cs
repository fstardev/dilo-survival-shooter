﻿using Player;

namespace Core
{
    public class MoveCommand: Command
    {
        private PlayerMovement m_playerMovement;
        private float m_h, m_v;

        public MoveCommand(PlayerMovement playerMovement, float h, float v)
        {
            m_playerMovement = playerMovement;
            m_h = h;
            m_v = v;
        }

        public override void Execute()
        {
            m_playerMovement.Move(m_h, m_v);
            m_playerMovement.Animating(m_h, m_v);
        }

        public override void UnExecute()
        {
            m_playerMovement.Move(-m_h, -m_v);
            m_playerMovement.Animating(m_h, m_v);
        }
    }
}