﻿using Player;

namespace Core
{
    public class ShootCommand: Command
    {
        private readonly PlayerShooting m_playerShooting;

        public ShootCommand(PlayerShooting playerShooting)
        {
            m_playerShooting = playerShooting;
        }

        public override void Execute()
        {
            m_playerShooting.Shoot();
        }

        public override void UnExecute()
        {
            //
        }
    }
}