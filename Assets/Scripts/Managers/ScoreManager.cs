﻿using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class ScoreManager : MonoBehaviour
    {
        public static int score;


        private Text m_text;


        private void Awake ()
        {
            m_text = GetComponent <Text> ();
            score = 0;
        }


        private void Update ()
        {
            m_text.text = "Score: " + score;
        }
    }
}
