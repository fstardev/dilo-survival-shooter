﻿using System.Collections;
using Enemy;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Managers
{
    public class EnemyManager : MonoBehaviour
    {
        [SerializeField] private PlayerHealth playerHealth;
        [SerializeField] private float spawnTime = 3f;
        [SerializeField] private Transform[] spawnPoints;

        [SerializeField] private MonoBehaviour factory;

        private IFactory Factory => factory as IFactory;

        private void Start()
        {
            // InvokeRepeating(nameof(Spawn), spawnTime, spawnTime);
            StartCoroutine(SpawnEnemyRoutine());
        }

        private IEnumerator SpawnEnemyRoutine()
        {
            
            while (playerHealth.currentHealth > 0f)
            {
                var spawnEnemy = Random.Range(0, 3);
                Factory.FactoryMethod(spawnEnemy, spawnPoints[spawnEnemy]);
                var count = (float)Factory.CountFactoryChild();
                var delayTime = Mathf.Max(spawnTime, count * 0.5f);
                // print($"delay: {delayTime} with {count}");
                yield return new WaitForSeconds(delayTime);
            }
        }

        private void Spawn()
        {
            if (playerHealth.currentHealth <= 0f)
            {
                return;
            }

            var spawnEnemy = Random.Range(0, 3);

            Factory.FactoryMethod(spawnEnemy, spawnPoints[spawnEnemy]);
        }
    }
}
