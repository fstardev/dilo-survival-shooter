﻿using Player;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Managers
{
    public class GameOverManager : MonoBehaviour
    {
        [SerializeField] private PlayerHealth playerHealth;       
        [SerializeField] private float restartDelay = 5f;
        [SerializeField] private Text warningText;


        private Animator m_anim;
        private float m_restartTimer;
        private static readonly int GameOver = Animator.StringToHash("GameOver");
        private static readonly int Warning = Animator.StringToHash("Warning");

        private void Awake()
        {
            m_anim = GetComponent<Animator>();
        }
        
        private void Update()
        {
            if (playerHealth.currentHealth > 0) return;
            m_anim.SetTrigger(GameOver);

            m_restartTimer += Time.deltaTime;

            if (m_restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        public void ShowWarning(float distance)
        {
            warningText.text = $"! {Mathf.RoundToInt(distance)} m";
            m_anim.SetTrigger(Warning);
        }
    }
}