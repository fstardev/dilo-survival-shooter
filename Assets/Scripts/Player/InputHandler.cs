﻿using System;
using System.Collections.Generic;
using Core;
using UnityEngine;

namespace Player
{
    public class InputHandler: MonoBehaviour
    {
        [SerializeField] private PlayerMovement playerMovement;
        [SerializeField] private PlayerShooting playerShooting;

        private Queue<Command> m_commands = new Queue<Command>();

        private PlayerHealth m_playerHealth;

        private void Awake()
        {
            m_playerHealth = GetComponent<PlayerHealth>();
        }

        private void FixedUpdate()
        {
            if (m_playerHealth.currentHealth <= 0) return;
            var moveCommand = InputMovementHandling();

            if (moveCommand == null) return;
            m_commands.Enqueue(moveCommand);
            moveCommand.Execute();
        }

        private void Update()
        {
            if (m_playerHealth.currentHealth <= 0) return;
            var shootCommand = InputShootHandling();
            shootCommand?.Execute();
        }

        
        private Command InputMovementHandling()
        {
            if (Input.GetKey(KeyCode.D))
            {
                return new MoveCommand(playerMovement, 1, 0);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                return new MoveCommand(playerMovement, -1, 0);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                return new MoveCommand(playerMovement, 0, 1);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                return new MoveCommand(playerMovement, 0, -1);
            }

            // else if (Input.GetKey(KeyCode.Z))
            // {
            //     return Undo();
            // }
            else
            {
                return new MoveCommand(playerMovement, 0, 0);
            }
        }

        private Command Undo()
        {
            if (m_commands.Count > 0)
            {
                var undoCommand = m_commands.Dequeue();
                undoCommand.UnExecute();
            }

            return null;
        }

        private Command InputShootHandling()
        {
            return Input.GetButtonDown("Fire1") ? new ShootCommand(playerShooting) : null;
        }
    }
}