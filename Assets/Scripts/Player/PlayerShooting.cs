﻿using System;
using Core;
using Enemy;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


namespace Player
{
    public class PlayerShooting : MonoBehaviour
    {
        [SerializeField] private int maxAmmo = 150;
        [SerializeField] private int ammo = 99;
        [SerializeField] private int damagePerShot = 20;
        [SerializeField] private float timeBetweenBullets = .15f;
        [SerializeField] private float range = 100f;
        [SerializeField] private int baseCritChance = 5;
        [SerializeField] private Text ammoText;
        [SerializeField] private Text statsText;

        private float m_timer;
        private Ray m_shootRay = new Ray();
        private RaycastHit m_shootHit;
        private int m_shootAbleMask;
        private ParticleSystem m_gunParticles;
        private LineRenderer m_gunLine;
        private AudioSource m_audio;
        private Light m_light;
        private const float EffectsDisplayTime = .2f;

        private bool m_infinityAmmo;

        private void Awake()
        {
            m_shootAbleMask = LayerMask.GetMask("ShootAble");
            m_gunParticles = GetComponent<ParticleSystem>();
            m_gunLine = GetComponent<LineRenderer>();
            m_audio = GetComponent<AudioSource>();
            m_light = GetComponent<Light>();
        }

        private void Start()
        {
            ammoText.color = Color.green;
            SetAmmoText();
            SetStatsText();
        }

        private void Update()
        {
            m_timer += Time.deltaTime;
            // if (Input.GetButton("Fire1") && m_timer >= timeBetweenBullets && Time.timeScale != 0)
            // {
            //     Shoot();
            // }

            if (m_timer >= timeBetweenBullets * EffectsDisplayTime)
            {
                DisableEffects();
            }
        }

     
        private void DisableEffects()
        {
            m_gunLine.enabled = false;
            m_light.enabled = false;
        }

        public void Shoot()
        {
            if (ammo <= 0) return;
            if (!(m_timer >= timeBetweenBullets) || Time.timeScale == 0) return;
            DecreaseAmmo();
            m_timer = 0;
            
            
            m_light.enabled = true;
            
            m_gunParticles.Stop();
            m_gunParticles.Play();
            
           
            var position = transform.position;

            m_gunLine.enabled = true;
            m_gunLine.SetPosition(0, position);
            
            m_shootRay.origin = position;
            m_shootRay.direction = transform.forward;

            m_audio.pitch = 1f;
            
            if (Physics.Raycast(m_shootRay, out m_shootHit, range, m_shootAbleMask))
            {
                const float minDistanceShootAblePlatformSound = 15;
                const float platformSoundPitch = 2f;
                const float enemySoundPitch = .5f;

                if (m_shootHit.distance < minDistanceShootAblePlatformSound)
                {
                    m_audio.pitch = platformSoundPitch;
                }
                
                var enemyHealth = m_shootHit.collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    var isCrit = Random.Range(0, 100) < baseCritChance;
                    var finalDamage = isCrit ? (damagePerShot * 2) : damagePerShot;
                    enemyHealth.TakeDamage(finalDamage, isCrit, m_shootHit.point);
                    m_audio.pitch = enemySoundPitch;
                }
                m_gunLine.SetPosition(1, m_shootHit.point);
               
            }
            else
            {
                m_gunLine.SetPosition(1, m_shootRay.origin + m_shootRay.direction * range);
               
            }
            
            m_audio.Play();
        }

        private void DecreaseAmmo()
        {
            if (m_infinityAmmo) return;
            ammo--;
            SetAmmoText();
        }

        private void SetAmmoText()
        {
            ammoText.text = $"{ammo:000}/{maxAmmo}";
        }

        private void SetStatsText()
        {
            statsText.text = $"dmg {damagePerShot}     %crit {baseCritChance}";
        }
        public void AddAmmo(int amount)
        {
            ammo = Mathf.Min(maxAmmo, ammo + amount);
            SetAmmoText();
        }

        public void UpgradeDamage()
        {
            damagePerShot++;
            SetStatsText();
        }

        public void IncreaseCritChance()
        {
            baseCritChance++;
            SetStatsText();
        }

        public void InfinityAmmo(bool state)
        {
            ammoText.color = state ? Color.magenta : Color.green;
            m_infinityAmmo = state;
        }
        
        
    }
}