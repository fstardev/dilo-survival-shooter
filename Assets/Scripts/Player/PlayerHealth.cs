﻿using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField] private int startingHealth = 100;
        [SerializeField] public int currentHealth;
        [SerializeField] private Slider healthSlider;
        [SerializeField] private Image damageImage;
        [SerializeField] private AudioClip deathClip;
        [SerializeField] private float flashSpeed = 5f;
        [SerializeField] private Color flashColour = new Color(1f, 0f, 0f, 0.1f);


        private Animator m_anim;
        private AudioSource m_playerAudio;

        private PlayerMovement m_playerMovement;
        private bool m_isDead;
        private bool m_damaged;
        private static readonly int Die = Animator.StringToHash("Die");


        private void Awake()
        {
            m_anim = GetComponent<Animator>();
            m_playerAudio = GetComponent<AudioSource>();
            m_playerMovement = GetComponent<PlayerMovement>();
            currentHealth = startingHealth;
        }


        private void Update()
        {
            if (m_damaged)
            {
                damageImage.color = flashColour;
            }
            else
            {
                damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
            }

            m_damaged = false;
        }


        public void TakeDamage(int amount)
        {
            m_damaged = true;

            currentHealth -= amount;

            healthSlider.value = currentHealth;

            m_playerAudio.Play();

            if (currentHealth <= 0 && !m_isDead)
            {
                Death();
            }
        }


        private void Death()
        {
            m_isDead = true;
        
            m_anim.SetTrigger(Die);

            m_playerAudio.clip = deathClip;
            m_playerAudio.Play();

            m_playerMovement.enabled = false;
        }

        public void RestartLevel()
        {
            print("Restarting Level");
        }

        public void IncreaseHealth(int health)
        {
            currentHealth = Mathf.Min(startingHealth, currentHealth + health);
            healthSlider.value = currentHealth;
        }
        
        
    }
}
