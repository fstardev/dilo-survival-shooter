﻿using System;
using Managers;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerDetector: MonoBehaviour
    {
        // [SerializeField] private GameOverManager gameOverManager;
        [SerializeField] private InfoTextController warningTextController;

        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy") && !other.isTrigger)
            {
                var distance = Vector3.Distance(transform.position, other.transform.position);
                // gameOverManager.ShowWarning(enemyDistance);
              
                warningTextController.SpawnText( $"! {Mathf.RoundToInt(distance)}m");
            }
        }
    }
}