﻿using System;
using UnityEngine;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float baseSpeed = 6f;
        [SerializeField] private float speed;
        
        private Vector3 m_movement;
        private Animator m_anim;
        private Rigidbody m_playerRigidbody;
        private int m_floorMask;
        private const float CamRayLength = 100f;
        private Camera m_camera;
        private static readonly int IsWalking = Animator.StringToHash("IsWalking");
        private Light m_poweredLight;
        private float m_counter;
        private PlayerShooting m_playerShooting;
        private void Awake()
        {
            m_camera = Camera.main;
            m_floorMask = LayerMask.GetMask("Floor");
            m_anim = GetComponent<Animator>();
            m_playerRigidbody = GetComponent<Rigidbody>();
            m_poweredLight = transform.GetChild(3).GetComponent<Light>();
            m_playerShooting = GetComponentInChildren<PlayerShooting>();
        }

        private void Start()
        {
            speed = baseSpeed;
            m_poweredLight.color = Color.cyan;
        }

        private void FixedUpdate()
        {
            // var h = Input.GetAxisRaw("Horizontal");
            // var v = Input.GetAxisRaw("Vertical");
            //Move(h, v);
            Turning();
            // Animating(h, v);
        }

        private void Update()
        {
            m_counter = Mathf.Max(0,  m_counter -= Time.deltaTime);
            if (!(m_counter <= 0)) return;
            speed = baseSpeed;
            m_poweredLight.color = Color.cyan;
            m_playerShooting.InfinityAmmo(false);

        }


        private void Turning()
        {
            var camRay = m_camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(camRay, out var floorHit, CamRayLength, m_floorMask))
            {
                var playerToMouse = floorHit.point - transform.position;
                playerToMouse.y = 0f;

                var newRotation = Quaternion.LookRotation(playerToMouse);
                m_playerRigidbody.MoveRotation(newRotation);
            }
        }

        public void Move(float h, float v)
        {
            m_movement.Set(h, 0f, v);
            m_movement = m_movement.normalized * (speed * Time.deltaTime);
            m_playerRigidbody.MovePosition(transform.position + m_movement);
        }

        public void Animating(float h, float v)
        {
            var walking = h != 0f || v != 0f;
            m_anim.SetBool(IsWalking, walking);
        }

        public void IncreaseSpeed(float moreSpeed, float duration)
        {
            speed = moreSpeed;
            m_counter = duration;
            m_poweredLight.color = Color.magenta;
            m_playerShooting.InfinityAmmo(true);
            m_playerShooting.AddAmmo(1);
        }

    }
}
