﻿using UnityEngine;

namespace CommandPatternDemo
{
    public abstract class CommandDemo
    {
        protected float moveDistance = 1f;
        public abstract void Execute(Transform boxTrans, CommandDemo commandDemo);
        public virtual void Undo(Transform boxTrans) {}
        public virtual void Move(Transform boxTrans) {}
    }

    public class MoveForward : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            Move(boxTrans);
            InputHandler.OldCommands.Add(commandDemo);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.forward * moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.forward * moveDistance);
        }
    }

    public class MoveReverse : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            Move(boxTrans);
            InputHandler.OldCommands.Add(commandDemo);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.forward * moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.forward * moveDistance);
        }
    }

    public class MoveLeft : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            Move(boxTrans);
            InputHandler.OldCommands.Add(commandDemo);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.right * moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.right * moveDistance);
        }
    }

    public class MoveRight : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            Move(boxTrans);
            InputHandler.OldCommands.Add(commandDemo);
        }

        public override void Undo(Transform boxTrans)
        {
            boxTrans.Translate(-boxTrans.right * moveDistance);
        }

        public override void Move(Transform boxTrans)
        {
            boxTrans.Translate(boxTrans.right * moveDistance);
        }
    }

    public class DoNothing : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
           // Do Nothing
        }
    }

    public class UndoCommandDemo : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            var oldCommands = InputHandler.OldCommands;

            if (oldCommands.Count > 0)
            {
                var latestCommand = oldCommands[oldCommands.Count - 1];
                latestCommand.Undo(boxTrans);
                oldCommands.RemoveAt(oldCommands.Count-1);
            }
        }
    }

    public class ReplayCommandDemo : CommandDemo
    {
        public override void Execute(Transform boxTrans, CommandDemo commandDemo)
        {
            InputHandler.shouldStartReplay = true;
        }
    }
}