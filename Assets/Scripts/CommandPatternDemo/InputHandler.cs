using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommandPatternDemo
{
    public class InputHandler : MonoBehaviour
    {
        [SerializeField] private Transform boxTrans;
        

        private CommandDemo m_buttonW, m_buttonS, m_buttonA, m_buttonD, m_buttonB, m_buttonZ, m_buttonR;

        public static readonly List<CommandDemo> OldCommands = new List<CommandDemo>();
        private Vector3 m_boxStartPos;
        private Coroutine m_replayCoroutine;
        public static bool shouldStartReplay;
        private bool m_isReplaying;

        private void Start()
        {
            m_buttonB = new DoNothing();
            m_buttonW = new MoveForward();
            m_buttonS = new MoveReverse();
            m_buttonA = new MoveLeft();
            m_buttonD = new MoveRight();
            m_buttonZ = new UndoCommandDemo();
            m_buttonR = new ReplayCommandDemo();

            m_boxStartPos = boxTrans.position;
        }

        private void Update()
        {
            if (!m_isReplaying)
            {
                HandleInput();
            }

            StartReplay();
        }

        private void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                m_buttonA.Execute(boxTrans, m_buttonA);
            }
            else if (Input.GetKeyDown(KeyCode.W))
            {
                m_buttonW.Execute(boxTrans, m_buttonW);
            }
            else if (Input.GetKeyDown(KeyCode.S))
            {
                m_buttonS.Execute(boxTrans, m_buttonS);
            }
            else if (Input.GetKeyDown(KeyCode.D))
            {
                m_buttonD.Execute(boxTrans, m_buttonD);
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                m_buttonB.Execute(boxTrans, m_buttonB);
            }
            else if (Input.GetKeyDown(KeyCode.Z))
            {
                m_buttonZ.Execute(boxTrans, m_buttonZ);
            } 
            else if (Input.GetKeyDown(KeyCode.R))
            {
                m_buttonR.Execute(boxTrans, m_buttonR);
            }
         
            
        }

        private void StartReplay()
        {
            if (shouldStartReplay && OldCommands.Count > 0)
            {
                shouldStartReplay = false;
                if (m_replayCoroutine != null)
                {
                    StopCoroutine(m_replayCoroutine);
                }

                m_replayCoroutine = StartCoroutine(ReplayCommandRoutine(boxTrans));
            }
        }

        private IEnumerator ReplayCommandRoutine(Transform box)
        {
            m_isReplaying = true;

            boxTrans.position = m_boxStartPos;

            foreach (var t in OldCommands)
            {
                t.Move(box);
                yield return new WaitForSeconds(.3f);
            }
            
            m_isReplaying = false;
        }
    }
}
