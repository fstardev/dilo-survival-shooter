﻿using Player;
using UnityEngine;

namespace PowerUp
{
    public class PowerHealth: Power
    {
        private readonly PlayerHealth m_playerHealth;
        private readonly int m_healthAmount;
        
        public PowerHealth(PlayerHealth playerHealth, int healthAmount)
        {
            m_playerHealth = playerHealth;
            m_healthAmount = healthAmount;
        }

        public override void InvokePower()
        {
            m_playerHealth.IncreaseHealth(m_healthAmount);
        }
        
    }
}