﻿using Player;

namespace PowerUp
{
    public class PowerSpeed: Power
    {
        private readonly PlayerMovement m_playerMovement;
        private readonly float m_speed;
        private readonly float m_duration;

        public PowerSpeed(PlayerMovement playerMovement, float speed, float duration)
        {
            this.m_playerMovement = playerMovement;
            this.m_speed = speed;
            this.m_duration = duration;
        }

        public override void InvokePower()
        {
            m_playerMovement.IncreaseSpeed( m_speed, m_duration);
        }
    }
}