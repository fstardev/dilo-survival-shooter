﻿using System;
using Player;
using UnityEngine;
namespace PowerUp
{
    public enum PowerType
    {
        Health,
        Speed,
        Ammo,
    }
    public class PowerHandler: MonoBehaviour
    {
        [SerializeField] private PowerType powerType = PowerType.Health;
        [SerializeField] private float spawnDelay = 20f;
        [SerializeField] private int health = 75;
        [SerializeField] private float duration;
        [SerializeField] private float speed;
        [SerializeField] private int ammo;
        
        private PowerHealth m_powerHealth;
        private PowerSpeed m_powerSpeed;
        private PowerAmmo m_powerAmmo;
        private AudioSource m_audio;

        private GameObject m_container;
        private float m_counter = 0;

        private void Awake()
        {

            m_container = transform.GetChild(0).gameObject;
            m_audio = GetComponentInParent<AudioSource>();
        }

        private void Update()
        {
            if (m_counter <= 0)
            {
                if (!m_container.activeSelf)
                {
                    m_audio.pitch = 3;
                    m_audio.Play();
                    m_container.SetActive(true);
                }
            }
            else
            {
                m_counter = Mathf.Max(0,  m_counter -= Time.deltaTime);
            }
           
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player") || other.isTrigger || !m_container.activeSelf) return;
            m_audio.pitch = 1;
            m_audio.Play();
            switch (powerType)
            {
                case PowerType.Health:
                {
                    var playerHealth = other.gameObject.GetComponent<PlayerHealth>();
                    m_powerHealth ??= new PowerHealth(playerHealth, health);
                    m_powerHealth.InvokePower();
                    break;
                }
                case PowerType.Speed:
                {
                    var playerMovement = other.gameObject.GetComponent<PlayerMovement>();
                    m_powerSpeed ??= new PowerSpeed(playerMovement, speed, duration);
                    m_powerSpeed.InvokePower();
                    break;
                }

                case PowerType.Ammo:
                    var playerShooting = other.gameObject.GetComponentInChildren<PlayerShooting>();
                    m_powerAmmo ??= new PowerAmmo(playerShooting, ammo);
                    m_powerAmmo.InvokePower();
                    break;
               
            }
            m_container.SetActive(false);
            m_counter = spawnDelay;


        }
    }
}