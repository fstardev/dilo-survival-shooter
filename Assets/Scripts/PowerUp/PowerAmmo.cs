﻿using Player;

namespace PowerUp
{
    public class PowerAmmo: Power
    {
        private readonly PlayerShooting m_playerShooting;
        private readonly int m_ammo;

        public PowerAmmo(PlayerShooting playerShooting, int ammo)
        {
            m_playerShooting = playerShooting;
            m_ammo = ammo;
        }

        public override void InvokePower()
        {
            m_playerShooting.AddAmmo(m_ammo);
            m_playerShooting.UpgradeDamage();
        }
    }
}