using UnityEngine;

namespace CameraController
{
   public class CameraFollow : MonoBehaviour
   {
      [SerializeField] private Transform target;
      [SerializeField] private float smoothing = 5f;

      private Vector3 m_offset;

      private void Start()
      {
         m_offset = transform.position - target.position;
      }

      private void FixedUpdate()
      {
         var targetCamPos = target.position + m_offset;
         transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
      }
   }
}
