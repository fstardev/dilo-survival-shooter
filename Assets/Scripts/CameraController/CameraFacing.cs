﻿using System;
using UnityEngine;

namespace CameraController
{
    public class CameraFacing: MonoBehaviour
    {
        private Camera m_camera;

        private void Awake()
        {
            m_camera = Camera.main;
        }

        private void Update()
        {
            transform.forward = m_camera.transform.forward;
        }
    }
}