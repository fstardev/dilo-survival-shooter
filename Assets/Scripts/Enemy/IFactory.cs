﻿using UnityEngine;

namespace Enemy
{
    public interface IFactory
    {
        GameObject FactoryMethod(int enemyTag, Transform enemyTransform);
        int CountFactoryChild();
    }
}