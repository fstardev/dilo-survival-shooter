﻿using System;
using UnityEngine;

namespace Enemy
{
    public class EnemyFactory: MonoBehaviour, IFactory
    {
        [SerializeField] private GameObject[] enemyPrefab;
        [SerializeField] private GameObject enemyParent;
        
        public GameObject FactoryMethod(int enemyTag, Transform enemyTransform)
        {
            var enemy = enemyPrefab[enemyTag];
            // print($"create enemy {enemy.name}");
           return Instantiate(enemy, enemyTransform.position, enemyTransform.rotation, enemyParent.transform);
        }

        public int CountFactoryChild()
        {
            return enemyParent.transform.childCount;
        }
        
    }
}