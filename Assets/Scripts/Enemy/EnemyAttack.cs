﻿using Player;
using UnityEngine;

namespace Enemy
{
    public class EnemyAttack : MonoBehaviour
    {
        [SerializeField] private float timeBetweenAttacks = .5f;
        [SerializeField] private int attackDamage = 10;

        private Animator m_anim;
        private GameObject m_player;
        private PlayerHealth m_playerHealth;
        private EnemyHealth m_enemyHealth;
        private bool m_playerInRange;
        private float m_timer;
        private static readonly int PlayerDead = Animator.StringToHash("PlayerDead");

        private void Awake()
        {
            m_anim = GetComponent<Animator>();
            m_player = GameObject.FindGameObjectWithTag("Player");
            m_playerHealth = m_player.GetComponent<PlayerHealth>();
            m_enemyHealth = GetComponent<EnemyHealth>();

        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player") && !other.isTrigger)
            {
                m_playerInRange = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                m_playerInRange = false;
            }
        }

        private void Update()
        {
            m_timer += Time.deltaTime;
            if (m_timer >= timeBetweenAttacks && m_playerInRange && m_enemyHealth.currentHealth > 0)
            {
                Attack();
            }

            if (m_playerHealth.currentHealth <= 0)
            {
                m_anim.SetTrigger(PlayerDead);
            }
        }

        private void Attack()
        {
            m_timer = 0f;
            if (m_playerHealth.currentHealth > 0)
            {
                m_playerHealth.TakeDamage(attackDamage);
            }
        }
    }
}
