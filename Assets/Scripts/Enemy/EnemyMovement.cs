﻿using Player;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        private Transform m_player;
        private PlayerHealth m_playerHealth;
        private EnemyHealth m_enemyHealth;
        private NavMeshAgent m_nav;


        private void Awake ()
        {
            m_player = GameObject.FindGameObjectWithTag ("Player").transform;
            m_playerHealth = m_player.GetComponent <PlayerHealth> ();
            m_enemyHealth = GetComponent <EnemyHealth> ();
            m_nav = GetComponent <NavMeshAgent> ();
        }


        private void Update ()
        {
            if (m_enemyHealth.currentHealth > 0 && m_playerHealth.currentHealth > 0)
            {
                m_nav.SetDestination(m_player.position);
            }
            else
            {
                m_nav.enabled = false;
            }
        }
    }
}
