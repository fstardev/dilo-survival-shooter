﻿using System;
using Managers;
using UI;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy
{
    public class EnemyHealth : MonoBehaviour
    {
        [SerializeField] private int startingHealth = 100;
        [SerializeField] internal int currentHealth;
        [SerializeField] private float sinkSpeed = 2.5f;
        [SerializeField] private int scoreValue = 10;
        [SerializeField] private AudioClip deathClip;
        
        [SerializeField] private InfoTextController critController;


        private Animator m_anim;
        private AudioSource m_audio;
        private ParticleSystem m_hitParticles;
        private CapsuleCollider m_collider;
        private bool m_isDead;
        private bool m_isSinking;
        private static readonly int Dead = Animator.StringToHash("Dead");

        private void Awake()
        {
            m_anim = GetComponent<Animator>();
            m_audio = GetComponent<AudioSource>();
            m_hitParticles = GetComponentInChildren<ParticleSystem>();
            m_collider = GetComponent<CapsuleCollider>();
        }

        private void Start()
        {
            currentHealth = startingHealth;
        }

        private void Update()
        {
            if (m_isSinking)
            {
                transform.Translate(-Vector3.up * (sinkSpeed * Time.deltaTime));
            }
        }

        public void TakeDamage(int amount, bool isCrit, Vector3 hitPoint)
        {
            if (m_isDead) return;

            if (isCrit)
            {
                critController.SpawnText($"Critical");
            }
            
            m_audio.Play();
            currentHealth -= amount;
            m_hitParticles.transform.position = hitPoint;
            m_hitParticles.Play();

            if (currentHealth <= 0)
            {
                Death();
            }
        }

        private void Death()
        {
            m_isDead = true;
            m_collider.isTrigger = true;
            m_anim.SetTrigger(Dead);
            m_audio.clip = deathClip;
            m_audio.Play();
        }

        public void StartSinking()
        {
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
            m_isSinking = true;
            ScoreManager.score += scoreValue;
            Destroy(gameObject, 2f);
        }
    }
}
