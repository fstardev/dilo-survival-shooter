﻿using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class InfoText: MonoBehaviour
    {
        [SerializeField] private Text warningText;

        public void  SetText(string text)
        {
            warningText.text = text;
        }
        
        public void DestroyText()
        {
            Destroy(gameObject);
        }
    }
}