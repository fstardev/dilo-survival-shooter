﻿using UnityEngine;

namespace UI
{
    public class InfoTextController: MonoBehaviour
    {
        [SerializeField] private InfoText prefab = null;


        public void SpawnText(string text)
        {
            var warningText = Instantiate<InfoText>(prefab, transform);
            warningText.SetText(text);
        }
        
        public void DestroyText()
        {
            Destroy(prefab);
        }
    }
}